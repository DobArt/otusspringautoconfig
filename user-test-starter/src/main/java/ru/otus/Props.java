package ru.otus;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix="application")
public class Props {

    private String lastName;
    private String firstName;
    private String bundlePath;
    private String fileTestPath;
    private String localePrefix;
    private Integer successCount;
}
