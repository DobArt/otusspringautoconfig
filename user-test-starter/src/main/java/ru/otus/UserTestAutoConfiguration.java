package ru.otus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import ru.otus.controller.ConsoleTestController;
import ru.otus.dao.UserTestDao;
import ru.otus.dao.UserTestDaoFromCSV;
import ru.otus.domain.UserTest;
import ru.otus.service.*;

import java.util.ResourceBundle;


@Configuration
@ConditionalOnClass(UserTest.class)
@EnableConfigurationProperties(Props.class)
public class UserTestAutoConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserTestAutoConfiguration.class);

    private final Props props;

    public UserTestAutoConfiguration(Props props) {
        this.props = props;
    }

    @Bean
    @ConditionalOnMissingBean
    public BundleService bundleService() {
        return new BundleService(props.getBundlePath(), props.getLocalePrefix());
    }

    @Bean
    @ConditionalOnMissingBean
    public UserTestDao userTestDao(ResourceLoader resourceLoader) {
        ResourceBundle bundle = bundleService().getBundle();
        Resource dataSource = resourceLoader.getResource(bundle.getString("resource.path"));
        return new UserTestDaoFromCSV(dataSource);
    }

    @Bean
    @ConditionalOnMissingBean
    public IOStreamService ioStreamService() {
        return new FileIOStreamService(props.getFileTestPath());
    }

    @Bean
    @ConditionalOnMissingBean
    public UserTestService userTestServiceImpl(ResourceLoader resourceLoader) {
        return new UserTestServiceImpl(userTestDao(resourceLoader),
                                       props.getSuccessCount(),
                                       ioStreamService(),
                                       bundleService().getBundle(),
                                       userTest()
        );
    }

    @Bean
    @ConditionalOnMissingBean
    public ConsoleTestController consoleTestController(ResourceLoader resourceLoader) {
        return new ConsoleTestController(userTestServiceImpl(resourceLoader), bundleService());
    }

    @Bean
    @ConditionalOnMissingBean
    public UserTest userTest() {
        String lastName = props.getLastName();
        String first = props.getFirstName();
        LOGGER.info(String.format("Created userTest for user: %s %s", lastName, first));
        return new UserTest(lastName, first);
    }
}
