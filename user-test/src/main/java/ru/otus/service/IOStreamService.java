package ru.otus.service;

import java.io.InputStream;
import java.io.OutputStream;

public interface IOStreamService {

    InputStream getInputStream();

    OutputStream getOutputStream();

}
