package ru.otus.service;

import org.springframework.stereotype.Service;

import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service("fileIOStreamService")
public class FileIOStreamService extends SystemIOStreamService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileIOStreamService.class);

    private final String path;

    public FileIOStreamService(String path) {
        this.path = path;
    }

    @Override
    public InputStream getInputStream() {
        try {
            return new FileInputStream(path + "answers.txt");
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
        return super.getInputStream();
    }

    @Override
    public OutputStream getOutputStream() {
        try {
            return new FileOutputStream(path + "resultTest.txt");
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
        return super.getOutputStream();
    }

}
