package ru.otus.service;


import lombok.Getter;
import org.springframework.stereotype.Service;
import ru.otus.domain.UserTest;
import ru.otus.domain.UserTestElement;
import ru.otus.dao.UserTestDao;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

@Service
public class UserTestServiceImpl implements UserTestService {

    private static final int HUNDRED = 100;

    private final Scanner scanner;
    private final UserTestDao dao;
    private final ResourceBundle bundle;
    private final int successTestCount;
    private final OutputStream out;
    @Getter
    private UserTest userTest;

    public UserTestServiceImpl(UserTestDao dao,
                               int successTestCount,
                               IOStreamService ioStreamService,
                               ResourceBundle bundle,
                               UserTest userTest) {
        this.dao = dao;
        this.successTestCount = successTestCount;
        this.scanner = new Scanner(ioStreamService.getInputStream());
        this.out = ioStreamService.getOutputStream();
        this.bundle = bundle;
        this.userTest = userTest;
    }

    @Override
    public void answerQuestions(UserTest userTest) {
        List<UserTestElement> userTestElements = dao.getUserTestElements();
        userTestElements.forEach(this::setAnswers);
        userTest.setTestElementList(userTestElements);
    }

    @Override
    public void printResult(UserTest userTest) {
        long countTrueAnswer = getCountTrueAnswer(userTest.getTestElementList());
        int size = userTest.getTestElementList().size();
        boolean isSuccessTest = countTrueAnswer * HUNDRED / size >= successTestCount;
        try {
            out.write((isSuccessTest ? bundle.getString("success.text") : bundle.getString("fail.text")).getBytes());
            out.write((String.format("\n" + bundle.getString("result.text") + "\n", userTest.getLastName(),
                    userTest.getFirstName(), countTrueAnswer, size)).getBytes());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        scanner.close();
    }

    /**
     * Метод добавляет ответ на текущий вопрос
     * @param userTestElement - объект содержащий вопрос
     */
    private void setAnswers(UserTestElement userTestElement) {
        try {
            out.write((userTestElement.getQuestion() + "\n").getBytes());
            userTestElement.setUserAnswer(scanner.nextLine());
            out.write(("Ответ: " + userTestElement.getUserAnswer() + "\n\n").getBytes());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Метод подситывает количество правильных ответов
     * @param userTestElementLists - список вопросов и ответов
     */
    private long getCountTrueAnswer(List<UserTestElement> userTestElementLists) {
        return userTestElementLists.stream().filter(it -> it.getTrueAnswer().equalsIgnoreCase(it.getUserAnswer())).count();
    }

}
