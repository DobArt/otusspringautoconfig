package ru.otus.service;

import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.io.OutputStream;

@Service("systemIOStreamService")
public class SystemIOStreamService implements IOStreamService {

    @Override
    public InputStream getInputStream() {
        return System.in;
    }

    @Override
    public OutputStream getOutputStream() {
        return System.out;
    }

}
