package ru.otus.service;


import ru.otus.domain.UserTest;

public interface UserTestService {

    UserTest getUserTest();

    /**
     * Метод добавляет ответы на вопросы тесты
     */
    void answerQuestions(UserTest userTest);

    /**
     * Метод выводит результаты тестирования
     */
    void printResult(UserTest userTest);
}
