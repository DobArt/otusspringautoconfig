package ru.otus.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Locale;
import java.util.ResourceBundle;

@Service
@RequiredArgsConstructor
public class BundleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BundleService.class);

    private final String pathToBundle;
    private final String localePrefix;

    public ResourceBundle getBundle() {
        Locale locale = StringUtils.isEmpty(localePrefix) ? Locale.getDefault() : Locale.forLanguageTag(localePrefix);
        return ResourceBundle.getBundle(pathToBundle, locale);
    }

}
