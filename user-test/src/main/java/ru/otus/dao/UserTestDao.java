package ru.otus.dao;

import ru.otus.domain.UserTestElement;

import java.util.List;

public interface UserTestDao {

    /**
     * Метод достает из ресурса список вопросов и ответов для теста
     */
    List<UserTestElement> getUserTestElements();

}
