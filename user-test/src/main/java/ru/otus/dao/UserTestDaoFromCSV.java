package ru.otus.dao;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import ru.otus.domain.UserTestElement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository("dao")
public class UserTestDaoFromCSV implements UserTestDao {

    private static final String SEPARATOR = ";";
    private static final int CORRECT_ARRAY_SIZE = 2;

    private final Resource dataSource;

    public UserTestDaoFromCSV(Resource resource) {
        this.dataSource = resource;
    }

    @Override
    public List<UserTestElement> getUserTestElements() {
        try (InputStream is = dataSource.getInputStream(); BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            return reader.lines()
                    .filter(line -> !StringUtils.isEmpty(line))
                    .map(this::createUserElement)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return Collections.emptyList();
    }

    /**
     * Метод создает элемент, содержащий вопрос и ответ
     * @param line - строка из файла
     */
    private UserTestElement createUserElement(String line) {
        String[] arr = line.split(SEPARATOR);
        return (arr.length == CORRECT_ARRAY_SIZE) ? new UserTestElement(arr[0], arr[1]) : null;
    }

}
