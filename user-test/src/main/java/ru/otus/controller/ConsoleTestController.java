package ru.otus.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.otus.domain.UserTest;
import ru.otus.service.BundleService;
import ru.otus.service.UserTestService;

import java.util.ResourceBundle;

@Controller
public class ConsoleTestController implements TestController {

    private final BundleService bundleService;
    private final UserTestService userTestService;

    @Autowired
    public ConsoleTestController(UserTestService userTestService, BundleService bundleService) {
        this.userTestService = userTestService;
        this.bundleService = bundleService;
    }

    @Override
    public void runTest() {
        System.out.println(bundleService.getBundle().getString("before.questions.text"));
        UserTest userTest = userTestService.getUserTest();
        userTestService.answerQuestions(userTest);
        userTestService.printResult(userTest);
    }
}
