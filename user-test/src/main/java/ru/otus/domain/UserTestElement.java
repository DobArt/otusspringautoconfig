package ru.otus.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Объект содержит вопрос, правильный ответ и ответ тестируемого человека
 */
@RequiredArgsConstructor
public class UserTestElement {

    @Getter
    private final String question;

    @Getter
    private final String trueAnswer;

    @Getter
    @Setter
    private String userAnswer;

}
