package ru.otus.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Объект теста
 */
@RequiredArgsConstructor
public class UserTest {

    @Getter
    private final String lastName;

    @Getter
    private final String firstName;

    @Getter
    @Setter
    private List<UserTestElement> testElementList;

}
