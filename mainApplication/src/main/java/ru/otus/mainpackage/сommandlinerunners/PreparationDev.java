package ru.otus.mainpackage.сommandlinerunners;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.otus.controller.TestController;

@Component
@RequiredArgsConstructor
public class PreparationDev implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(PreparationDev.class);

    private final TestController testController;

    @Override
    public void run(String... args) {
        logger.info("Test start!");
        testController.runTest();
        logger.info("Test finish!");
    }
}
