package ru.otus.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.otus.domain.UserTestElement;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@SpringBootTest()
@RunWith(SpringRunner.class)
public class UserTestDaoFromCSVTest {

    private final UserTestDao userTestDao;

    @Autowired
    public UserTestDaoFromCSVTest(UserTestDao userTestDao) {
        this.userTestDao = userTestDao;
    }

    @Test
    public void getUserTestElementsTest() {
        List<UserTestElement> userTestElements = userTestDao.getUserTestElements();
        assertThat(userTestElements.isEmpty()).isFalse();
        assertThat(userTestElements.size()).isEqualTo(5);
    }

}
