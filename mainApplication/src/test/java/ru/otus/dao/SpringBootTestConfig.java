package ru.otus.dao;


import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

@SpringBootConfiguration
@Configuration
public class SpringBootTestConfig {

    private static final String CLASSPATH_TESTS_TEST_01_RU_RU_CSV = "classpath:/tests/test01_ru-RU.csv";

    @Bean
    public UserTestDao userTestDao(ResourceLoader resourceLoader) {
        Resource dataSource = resourceLoader.getResource(CLASSPATH_TESTS_TEST_01_RU_RU_CSV);
        return new UserTestDaoFromCSV(dataSource);
    }

}
